{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "tuto38lib";
  version = "1.0";
  #src = builtins.path { path = ./.; name = "source"; };
  src = ./.;
  buildInputs = with pkgs; [ cmake ];
}

